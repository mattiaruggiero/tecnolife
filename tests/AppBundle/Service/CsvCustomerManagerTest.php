<?php

namespace Tests\AppBundle\Service;

use AppBundle\Service\CsvCustomerManager;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CsvCustomerManagerTest extends KernelTestCase
{
    private $csvCustomerManager;

    protected function setUp()
    {
        self::bootKernel();
        $this->csvCustomerManager = static::$kernel->getContainer()->get('AppBundle\Service\CustomerManagerInterface');
    }

    public function testGetCustomerById()
    {
        $customer = $this->csvCustomerManager->getCustomerById("1");

        $this->assertNotNull($customer);
        $this->assertInstanceOf('AppBundle\Model\Customer', $customer);
        $this->assertEquals('01/04/2015', $customer->getTransactions()[0]->getDate());
        $this->assertCount(4, $customer->getTransactions());

        $customer = $this->csvCustomerManager->getCustomerById("3");
        $this->assertFalse($customer);
    }
}