<?php

namespace Tests\AppBundle\Service;

use AppBundle\Service\CurrencyConverter;
use AppBundle\Service\CurrencyWebservice;
use PHPUnit\Framework\TestCase;

class CurrencyConverterTest extends TestCase
{
    public function testConvert()
    {
        $currencyWebservice = $this->createMock(CurrencyWebservice::class);
        $currencyConverter = new CurrencyConverter($currencyWebservice);

        $result = $currencyConverter->convert("£", "EUR", 30);
        $expectedArray = [
            "symbol" => "€",
            "amount" => is_numeric($result["amount"])
        ];
        // assert that your currency converter convert the amount correctly!
        $this->assertEquals($expectedArray, $result);

        $result = $currencyConverter->convert("£", "JPY", 30);
        // assert that your currency converter return false if currency not exist
        $this->assertFalse($result);

    }
}