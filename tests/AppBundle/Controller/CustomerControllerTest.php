<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CustomerControllerTest extends WebTestCase
{
    public function testGetCustomerTransactions()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/customers/1/transactions');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request('GET', '/customers/3/transactions');
        // Assert that the response status code is 404
        $this->assertTrue($client->getResponse()->isNotFound());

        $client->request('GET', '/customers/1/transactions?currency=§');
        // Assert that the response status code is 404
        $this->assertTrue($client->getResponse()->isNotFound());

    }
}
