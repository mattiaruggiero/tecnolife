<?php

namespace AppBundle\Service;

/**
 * Dummy web service returning random exchange rates.
 *
 * Class CurrencyWebservice
 * @package AppBundle\Service
 */
class CurrencyWebservice
{

    /**
     * Return random number.
     *
     * @param $fromCurrency
     * @param $toCurrency
     * @param $amount
     * @return float|int
     */
    public function getExchangeRate($fromCurrency, $toCurrency, $amount)
    {
        return mt_rand(10, 1000*10) / 100;
    }
}