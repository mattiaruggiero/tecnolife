<?php

namespace AppBundle\Service;

/**
 * Interface to be implemented by customer managers. This adds an additional level
 * of abstraction between your application, and the actual repository.
 *
 * All changes to customer should happen through this interface.
 *
 * @author Mattia Ruggiero
 */
interface CustomerManagerInterface
{

    /**
     * Get customer by the given id
     *
     * @param string $id
     *
     * @return Customer|bool
     */
    public function getCustomerById($id);

}