<?php

namespace AppBundle\Service;

/**
 * This class is used to map symbols into currency and convert an amount from a currency to another.
 * Uses CurrencyWebservice
 *
 * Class CurrencyConverter
 * @package AppBundle\Service
 */
class CurrencyConverter
{
    /**
     * @var CurrencyWebservice
     */
    private $currencyWebservice;

    /**
     * This defines the mapping between symbols (key) and currency (value).
     *
     * @var array
     */
    private $symbolCurrencyMap = [
        "£" => "GBP",
        "$" => "USD",
        "€" => "EUR"
    ];

    /**
     * CurrencyConverter constructor.
     * @param CurrencyWebservice $currencyWebservice
     */
    public function __construct(CurrencyWebservice $currencyWebservice)
    {
        $this->currencyWebservice = $currencyWebservice;
    }

    /**
     * Return converted amount and associated currency symbol.
     *
     * @param $fromCurrency
     * @param $toCurrency
     * @param $amount
     *
     * @return array|bool
     */
    public function convert($fromCurrency, $toCurrency, $amount)
    {

        $toSymbol = array_search($toCurrency, $this->symbolCurrencyMap);
        if (false === $toSymbol) {
            return false;
        }

        $from = $this->symbolCurrencyMap[$fromCurrency];
        $amount = $this->currencyWebservice->getExchangeRate($from, $toCurrency, $amount);

        $array = [
            "symbol" => $toSymbol,
            "amount" => $amount
        ];
        return $array;
    }
}