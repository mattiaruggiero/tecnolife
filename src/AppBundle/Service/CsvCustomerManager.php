<?php

namespace AppBundle\Service;

use AppBundle\Model\Customer;
use AppBundle\Model\Transaction;
use AppBundle\Service\CustomerManagerInterface;

/**
 * Class CustomerManager
 * @package AppBundle\Service
 */
class CsvCustomerManager implements CustomerManagerInterface
{
    /**
     * @var array transactions
     */
    private $data = array();

    /**
     * @var string
     */
    private $dataDir;

    /**
     * CustomerManager constructor.
     *
     * @param $dataDir
     */
    public function __construct($dataDir)
    {
        $this->dataDir = $dataDir;
        $this->data = array_map(function($d) {
            return str_getcsv($d, ";");
        }, file($this->dataDir));
    }

    /**
     * Get customer by the given id from csv file
     *
     * @param $id
     *
     * @return Customer|bool
     */
    public function getCustomerById($id)
    {
        $transactions = array();
        foreach ($this->data as $row){
            if($row[0]==$id){
                if(!isset($customer)){
                    $customer = new Customer();
                    $customer->setId($row[0]);
                }
                $t = new Transaction();
                $t->setCustomerId($customer->getId());
                $t->setDate($row[1]);
                $t->setValue(mb_substr($row[2],1));
                $t->setCurrency(mb_substr($row[2],0,1));
                array_push($transactions, $t);
            }
        }
        if (!isset($customer)) {
            return false;
        }
        $customer->setTransactions($transactions);
        return $customer;
    }

}
