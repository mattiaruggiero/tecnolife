<?php
/**
 * Created by PhpStorm.
 * User: Mattia
 * Date: 26/10/2017
 * Time: 15:16
 */

namespace AppBundle\Model;

class Customer
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var Transaction[]
     */
    private $transactions;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Transaction[]
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * @param Transaction[] $transactions
     */
    public function setTransactions($transactions)
    {
        $this->transactions = $transactions;
    }


}