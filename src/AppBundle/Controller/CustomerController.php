<?php

namespace AppBundle\Controller;

use AppBundle\Service\CurrencyConverter;
use AppBundle\Service\CustomerManager;
use AppBundle\Service\CustomerManagerInterface;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Rest controller for customer
 *
 * @package AppBundle\Controller
 * @author Mattia Ruggiero
 */
class CustomerController extends FOSRestController
{

    /**
     * return \AppBundle\Service\CustomerManagerInterface
     */
    public function getCustomerManagerInterface()
    {
        return $this->get(CustomerManagerInterface::class);
    }

    /**
     * return \AppBundle\Service\CurrencyConverter
     */
    public function getCurrencyConverter()
    {
        return $this->get(CurrencyConverter::class);
    }

    /**
     * List customer's transactions.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the customer is not found"
     *   }
     * )
     *
     * @Annotations\QueryParam(name="currency", nullable=true, description="Currency to show the transactions with")
     * @Annotations\View(templateVar="transactions")
     *
     * @param int $id the customer id
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     *
     * @throws NotFoundHttpException when customer not exist
     * @throws NotFoundHttpException when currency not exist
     */
    public function getCustomerTransactionsAction($id, ParamFetcherInterface $paramFetcher)
    {
        $customerManagerInterface = $this->getCustomerManagerInterface();
        $customer = $customerManagerInterface->getCustomerById($id);
        if (false === $customer) {
            throw $this->createNotFoundException("Customer does not exist.");
        }

        $toCurrency = $paramFetcher->get('currency');
        $toCurrency = null == $toCurrency ? "EUR" : $toCurrency;

        foreach ($customer->getTransactions() as $transaction){
            $amount = $transaction->getValue();
            $fromCurrency = $transaction->getCurrency();
            $currencyConverterObj = $this->getCurrencyConverter()->convert($fromCurrency, $toCurrency, $amount);
            if(false === $currencyConverterObj){
                throw $this->createNotFoundException("Currency is not valid.");
            }
            $transaction->setValue($currencyConverterObj["amount"]);
            $transaction->setCurrency($currencyConverterObj["symbol"]);
        }

        return $customer->getTransactions();
    }

}
