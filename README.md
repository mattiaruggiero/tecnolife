Tecnolife - Symfony Rest Application
========================

This document contains information on how to download, install, and start
Tecnolife Symfony Rest Application. For a more detailed explanation,
see the [Installation][1] chapter of the Symfony Documentation.

1) Installing the Application
----------------------------------

### Download an Archive File

To test Application, you can download the [archive][2]
of the project and unpack it somewhere under your web server root directory.

### Clone repository
To test Application you can also clone the repository on your local machine:

    git clone https://gitlab.com/mattiaruggiero/tecnolife.git
    cd tecnolife

If you downloaded or checkout the project "without vendors",
you also need to install all the necessary dependencies.
Download composer [http://getcomposer.org] and run the following command:

    php composer.phar install

2) Checking your System Configuration
-------------------------------------

Before starting coding, make sure that your local system is properly
configured for Symfony.

Access the `config.php` script from a browser:

    http://127.0.0.1:8000/config.php

If you get any warnings or recommendations, fix them before moving on.

3) Browsing the Application
---------------------------

On your local development machine you can use the web server provided
by Symfony, which in turn uses the built-in web server provided by PHP

execute this command:

    php bin/console server:run
    
To see a real-live Symfony page in action and to test API, 
access the following page:

    http://127.0.0.1:8000/customers/1/transactions
    http://127.0.0.1:8000/customers/1/transactions?currency=EUR
    http://127.0.0.1:8000/customers/1/transactions?currency=USD
    http://127.0.0.1:8000/customers/1/transactions?currency=GBP

You can browse the API documentation at:

    http://127.0.0.1:8000/api/doc

4) Test
-------

To run the tests install PHPUnit 6.4+ and call:

    phpunit or vendor/bin/phpunit

    
Enjoy!

[1]:  https://symfony.com/doc/current/setup.html
[2]:  https://gitlab.com/mattiaruggiero/tecnolife/repository/master/archive.zip